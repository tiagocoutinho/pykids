#  sessao4.py
# .lower() cria um novo texto todo em minusculas
jogos = ["Lego Mindstorms", "Fortnite", "Pokemon", "Super Mario"]
jogos_minusculas = [jogo.lower() for jogo in jogos]

print("Qual e' o teu jogo favorito?")
resposta = input()
resposta_minuscula = resposta.lower()
if resposta_minuscula in jogos_minusculas:
    print("boa resposta")
else:
    print("eu nao conheco esse jogo")
