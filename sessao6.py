#  sessao4.py
# .lower() cria um novo texto todo em minusculas
# while equanto a condicao e verdade
jogos = ["Lego Mindstorms", "Fortnite", "Pokemon", "Super Mario"]
jogos_minusculas = [jogo.lower() for jogo in jogos]
jogo_encontrado = False
while not jogo_encontrado:

    print("Qual e' o teu jogo favorito?")
    resposta = input()
    resposta_minuscula = resposta.lower()

    jogo_encontrado = resposta_minuscula in jogos_minusculas
    if not jogo_encontrado:
        print("eu nao conheco esse jogo tenta outra vez.")

print("boa resposta")
