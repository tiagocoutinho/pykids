name: empty layout
layout: true

---
class: center, middle

PyKids
======

Python para jovens

•

[Gitlab](https://gitlab.com/tiagocoutinho/pykids) - [Slides](http://tiagocoutinho.gitlab.io/pykids)

---

# Plano

1. Instalar Python
2. Lançar um terminal
3. Primeiras experiências

---

# Instalar python (...em Windows)

.center[![Download](site_download.png)]


1. Ir ao site [python.org/download](https://www.python.org/downloads)
2. Carregar no botão **Download Python 3...**
3. Escolher **Salvar o ficheiro**
4. Pedir para executar o ficheiro
5. Escolher a opção *Add to PATH* e fazer a instalação
   (pode demorar alguns minutos)

---

# Lançar um terminal python

No windows, executar o programa chamado *cmd*...

... e escrever **python [Enter]**:

<pre style="background-color:#000000;color:#ffffff">
 Micro$oft Windows [Version 10.0.10240]
 (c) 1998 Micro$oft Coorporation. No rights reserved

C:\> <span style="color:#FF0000;font-weight:bold;">python</span>

Python 3.7.5 (default, Oct 25 2019, 15:51:11)
[GCC 7.3.0] :: Anaconda, Inc. on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>


</pre>

.center[**Parabéns!** 🥳 Acabaste de entrar no mundo maravilhoso do Python.]

---

# Primeiras experiências

Vamos escrever o nosso primeiro código:

```python
>>> print("Olá Sebastião!")
Olá Sebastião!
```

Podemos o nosso text numa *variavel* e usarmos sempre que queremos:

```python
>>> texto = "Olá Sebastião!"
>>> print(texto)
```

Também podemos fazer contas de matemática

```python
>>> print(23 + 45)
68
```

---

# Mais experiências

Vamos criar uma lista dos nossos jogos:

```python
>>> jogos = ["Lego Mindstorms", "Fortnite", "Pokemon"]
```

... e pedir ao python que imprima um jogo de cada vez:

```python
>>> for jogo in jogos:
...     print(jogo)
Lego Mindstorms
Fortnite
Pokemon
```

---

# Exemplo complicado

Agora algo mais complicado: Imprimir aquele jogo que gostamos mais (*Fortine*, claro 😉)
com uma mensage especial e os outros jogos com outra mensagem:

```python
>>> for jogo in jogos:
...     if jogo == "Fortnite":
...         print("Eu gosto de jogar " + jogo + ": é o meu favorito")
...     else:
...         print("Eu gosto de jogar " + jogo)
Eu gosto de jogar Lego Mindstorms
Eu gosto de jogar Fortnite: é o meu favorito
Eu gosto de jogar Pokemon
```


... é uma pena perdemos o código que nos deu tanto trabalho a escrever, não é? 🤔

Seria muito melhor se pudéssemos gravar num ficheiro para poder continuar a melhorar
no dia seguinte.

---

# Recapitulamos...


* `print()` é uma função que imprime no ecrã aquilo pomos dentro de `()`

* para imprimir texto temos de por " no começo e no fim do texto:
  ```python
  print("Bom dia")`
  ```

* variavéis ajudam a guardar valores
  ```python
  texto = "Bom dia!"
  ```

* `for ... in ...:` ajuda-nos a executar instruções várias vezes:
  ```python
  for jogo in ["Fortnite", "Pokemon"]:
      print(jogo)
  ```

* `if ` só executa uma instrução se a condição for verdade
  ```python
  jogo = "Mario"
  if jogo == "Fortnite":
      print("Este é o meu jogo favorito")
  ```

---

class: middle

# Guardar ficheiros

Agora que já sabemos escrever código em Python é altura de aprendermos a
guardar o nosso código num ficheiro.

Os ficheiros Python terminam com a extensão **.py** (exemplo: `jogo.py`).

Mas antes precisamos um editor de texto apropriado!

Vamos fazer download e instalar um editor que se chama **notepad++**.

---

# Instalar o notepad++

Vamos ao site [notepad-plus-plus.org/downloads/](https://notepad-plus-plus.org/downloads/)

Escolhemos a versão mais recente e fazemos download.

Abrimos o notepad++

---

# O primeiro ficheiro

Escrevemos no notepad:

```python
# sessao2.py

jogos = ["Lego Mindstorms", "Fortnite", "Pokemon"]

print("Os meus jogos favoritos são:")

for jogo in jogos:
    print(jogo)
```

... e salvamos com o nome **sessao2.py**

abrimos agora o programa **cmd** e escrevemos `python sessao2.py`:

<pre style="background-color:#000000;color:#ffffff">


C:\> <span style="color:#FFFFFF;font-weight:bold;">python sessao2.py</span>
Os meus jogos favoritos são:
Lego Mindstorms
Fortnite
Pokemon

</pre>


---

# Juntamos o **for** e o **if**

1. Abrir o notepad++ e criar um novo ficheiro

2. Escrever o código:

```python
# sessao2.py

jogos = ["Lego Mindstorms", "Fortnite", "Pokemon"]

print("--- Programa de jogos ---")

for jogo in jogos:
    if jogo == "Fortnite":
        print("Eu gosto de jogar " + jogo + ": é o meu favorito")
    else:
        print("Eu gosto de jogar " + jogo)
```

.center[(continua no próximo slide...)]

---

... e salvamos com o nome **sessao3.py** no teu directório **sebas**

Abrimos agora o programa **cmd** e mudamos para o directório **sebas** com: `cd\users\ricardo\desktop\sebas`

Executamos o nosso programa escrevendo `python sessao3.py`:

<pre style="background-color:#000000;color:#ffffff">


C:\> <span style="color:#FFFFFF;font-weight:bold;">cd\users\ricardo\desktop\sebas</span>
C:\> <span style="color:#FFFFFF;font-weight:bold;">python sessao3.py</span>
--- Programa de jogos ---
Eu gosto de jogar Lego Mindstorms
Eu gosto de jogar Fortnite: é o meu favorito
Eu gosto de jogar Pokemon

</pre>

---

## Jogo de adivinhas

![esquema](esquema1.jpg)
