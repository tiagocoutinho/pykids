# int() traduz texto a numero inteiro
# elif <condicao> quer dizer: caso contrario se <condicao> e verdade
# while True repete o codigo indentado
# import ler a biblioteca
# break para o ciclo while

# == comparacao de valores

import random
numero_aleatorio = random.randint(1, 10)
print("Adivinha o numero em que estou a pensar entre 1 e 10 ")
while True:
    resposta = input()
    resposta_int = int(resposta)
    if resposta_int == numero_aleatorio:
        print("Acertaste!!!")
        break
    elif resposta_int < numero_aleatorio:
        print("Esse numero e muito pequeno.Tenta outra vez.")
    else:
        print("Esse numero e demasiado grande.Tenta outra vez.")
