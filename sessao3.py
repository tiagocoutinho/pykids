# sessao3.py

jogos = ["Lego Mindstorms", "Fortnite", "Pokemon"]

print("--- Programa de jogos ---")

for jogo in jogos:
    if jogo == "Fortnite":
        print("Eu gosto de jogar " + jogo + ": é o meu favorito")
    else:
        print("Eu gosto de jogar " + jogo)
